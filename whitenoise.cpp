/*
**  White Noise genarete a gaussian white noise vector to use
**  on anywhere you whish
**
**  This files are BSD license, but you need keep my name how 
**  primary author
**
**	Author:  Nilton Jose Rizzo
** date:    24/01/2019
**
*/


#include <iostream>
#include <iomanip>
#include <random>
#include <vector>
#include <fstream>

#include <whitenoise.hpp>

using namespace std;

WhiteNoise::WhiteNoise (){

	WhiteNoise(0.0,1.0,10,false);
}

WhiteNoise::WhiteNoise (double m, double v, long s, bool snr){
    this->mean = m;
    this->var = v;
    this->num = s;
    this->withsnr = snr;
}

WhiteNoise::WhiteNoise (double SNR, long s){
    this->mean = 0.0;
    this->var = 1.0;
    this->num = s;
    this->sigma = sqrt(pow(10,(-SNR/10)));
    this->withsnr = true;
}

vector<double> WhiteNoise::generateNoiseValues(unsigned long int numberofvalues){
	this->setNumberOfValues(numberofvalues);
	return this->generateNoiseValues();
}

vector<double> WhiteNoise::generateNoiseValues(){
    
    default_random_engine defaultGeneratorEngine;
    normal_distribution<double> normalDistribution(this->mean,this->var);
    
    if (this->withsnr) {
        for (int i = 0; i<this->num; i++) {
			double s;
			s  = (this->sigma) * normalDistribution(defaultGeneratorEngine);
            this->noiseVector.push_back(s);
        }
    } else {
        for (int i = 0; i<this->num; i++) {
			double s;
			s = normalDistribution(defaultGeneratorEngine);
            this->noiseVector.push_back(s);
        }
    }
    return this->noiseVector;
}


bool WhiteNoise::saveValues(string fn){
	fstream	f;
	bool r;
	
	r=false;
	f.open(fn, ios_base::out | ios_base::binary);
	if (f.is_open()){
		f << MAGIC_NUMBER << endl;
		for (unsigned int i=0; i < this->num; i++)
			f << setprecision(6) << right << noiseVector[i];
		r = true;
	}
	return r;
}

void WhiteNoise::showValues(bool newLine){

	for (unsigned int i=0; i < this->num; i++ ){
		cout << setprecision(6) << setw(10) << right << noiseVector[i];
		if ( newLine )
			cout << endl;
		else
			cout << " ";
	}
}



WhiteNoise::~WhiteNoise(){
	if (noiseVector.empty())
		noiseVector.clear();
}

